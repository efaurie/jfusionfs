package edu.iit.jfusionfs.executable;

import java.util.HashMap;
import java.util.Map;

import javax.swing.SwingUtilities;

import edu.iit.jfusionfs.gui.JFusionFrame;
import edu.iit.jfusionfs.lib.FileTransferDaemon;
import edu.iit.jfusionfs.lib.JFusionConfigReader;

public class JFusionFS {
	private static final String DEFAULT_SHOW_GUI = "True";
	
	private static FileTransferDaemon fileTransferDaemon;
	
	public JFusionFS() {
		
	}
	
	private static Map<String, String> parseArgs(String[] args) {
		Map<String, String> parameters = new HashMap<String, String>();
		
		parameters.put("showGUI", DEFAULT_SHOW_GUI);
		
		for(int i = 0; i < args.length; i++) {
			if(args[i].equals("-config")) {
				parameters.put("config", args[i+1]);
				i++;
			} else if(args[i].equals("-nogui")) {
				parameters.put("showGUI", "False");
			}
		}
		
		return parameters;
	}
	
	private static void showGUI() {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				JFusionFrame jFusionTerminal = new JFusionFrame();
				jFusionTerminal.display();
			}
		});
		
		// Give the GUI Time to take control of STDOUT
		try {
			Thread.sleep(1000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
	
	private static void startFileTransferDaemon(int port, String workspace) {
		fileTransferDaemon = new FileTransferDaemon(port, workspace);
		new Thread(fileTransferDaemon).start();
	}
	
	public static void main(String[] args) {
		Map<String, String> parameters = parseArgs(args);
		
		if(parameters.get("showGUI").equals("True"))
			showGUI();
		
		JFusionConfigReader config;
		if(parameters.containsKey("config"))
			config = new JFusionConfigReader(parameters.get("config"));
		else
			config = new JFusionConfigReader();
		
		startFileTransferDaemon(config.getFileTransferPort(), config.getWorkspace());
	}

}
