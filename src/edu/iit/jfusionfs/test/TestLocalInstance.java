package edu.iit.jfusionfs.test;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.util.Arrays;

import edu.iit.jfusionfs.lib.JFusionFSInstance;

public class TestLocalInstance {
	private static final String TEST_FILE_PATH = "testing/local_instance.bin";
	private static final int TEST_FILE_SIZE = 1000;
	
	private static JFusionFSInstance jFusionFS;
	private static TestDataGenerator generator;
	
	public static void main(String[] args) {
		printTestHeader();
		System.out.println("[+] Instantiating JFusionFS Instance");
		jFusionFS = new JFusionFSInstance();
		System.out.println("[+] JFusionFS Instance Initialized\n");
		
		generator = new TestDataGenerator();
		
		System.out.println("[Data] Generating Test Data");
		byte[] dataToWrite = generator.getByteArrayOfSize(TEST_FILE_SIZE);
		System.out.println("[Data] Random Byte Array of Size " + Integer.toString(TEST_FILE_SIZE) + " Generated\n");
		
		System.out.println("[ffsWrite] Writing File '" + TEST_FILE_PATH +"'");
		writeData(dataToWrite);
		System.out.println("[ffsWrite] File Successfully Written\n");
		
		System.out.println("[ffsRead] Reading File '" + TEST_FILE_PATH + "'");
		BufferedInputStream bufferedStream = readData(TEST_FILE_PATH);
		System.out.println("[ffsRead] File Successfully Read\n");
		
		System.out.println("[Data] Comparing Read Data With Generated Data...");
		if(dataIsTheSame(dataToWrite, bufferedStream))
			System.out.println("[+] Data Compare: Pass");
		else
			System.out.println("[-] Data Compare: Failed");
	}
	
	private static void writeData(byte[] dataToWrite) {
		jFusionFS.ffsWrite(TEST_FILE_PATH, dataToWrite);
	}
	
	private static BufferedInputStream readData(String filePath) {
		return jFusionFS.ffsRead(filePath);
	}
	
	private static boolean dataIsTheSame(byte[] generatedData, BufferedInputStream bufferedStream) {
		byte[] readData = new byte[generatedData.length];
		
		try {
			bufferedStream.read(readData);
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		if(Arrays.equals(generatedData, readData))
			return true;
		else
			return false;
	}
	
	private static void printTestHeader() {
		String header = "";
		
		header += "Test JFusionFS Local File I/O";
		header += "\n\nConfig Requirements:";
		header += "\n   workspace=<directory on local machine>";
		header += "\n\nSteps:";
		header += "\n   1. Configure Workspace (set it to a directory on local machine)";
		header += "\n   2. Run TestLocalInstance on Local Machine";
		header += "\n   3. Ensure file <workspace>\\testing\\local_instance.bin was created";
		header += "\n\n-----BEGIN TEST-----\n";
		
		System.out.println(header);
	}

}
