package edu.iit.jfusionfs.test;

import java.io.File;

import edu.iit.jfusionfs.lib.JFusionFSInstance;

public class TestReadFile {
	private static final String FILEPATH = "testing/LFS_instance.bin";
	
	public static void main(String[] args) {
		JFusionFSInstance jfusionfs = new JFusionFSInstance();
		File file = jfusionfs.ffsOpen(FILEPATH);
	}

}
