package edu.iit.jfusionfs.test;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import edu.iit.jfusionfs.lib.JFusionConfigReader;
import edu.iit.jfusionfs.lib.JFusionFSInstance;

public class Benchmark {
	private static final int DEFAULT_SIZE = 1000;
	private static final int DEFAULT_COUNT = 100;
	private static final String DEFAULT_TYPE = "write";
	
	private static Map<String, String> args;
	private static TestDataGenerator generator;
	private static JFusionConfigReader config;
	
	
	public static void main(String[] givenArgs) {
		parseArgs(givenArgs);
		generator = new TestDataGenerator();
		config = new JFusionConfigReader();
		
		benchmark(args.get("type"), Integer.parseInt(args.get("count")), Integer.parseInt(args.get("size")));
	}
	
	private static void parseArgs(String[] givenArgs) {
		args = new HashMap<String, String>();
		
		args.put("size", Integer.toString(DEFAULT_SIZE));
		args.put("count", Integer.toString(DEFAULT_COUNT));
		args.put("type", DEFAULT_TYPE);
		
		for(int i = 0; i < givenArgs.length; i++) {
			if(givenArgs[i].contains("-")) {
				args.put(givenArgs[i].replace("-", ""), givenArgs[i+1]);
				i += 1;
			}
		}
	}
	
	private static void benchmark(String type, int fileCount, int size) {
		if(type.equals("write")) {
			byte[] data = generator.getByteArrayOfSize(size);
			
			benchmarkJFusionWrite(fileCount, data);
			benchmarkVanillaWrite(fileCount, data);
		}
	}
	
	private static void benchmarkJFusionWrite(int fileCount, byte[] data) {
		JFusionFSInstance jFusionFS = instantiateJFusion();
		
		System.out.println("\n\nWriting Data With JFusionFS");
		String filePath = "benchmark/jfusion_write_";
		String thisPath = "";
		
		long startTime = System.nanoTime();
		for(int i = 0; i < fileCount; i++) {
			thisPath = filePath + i + ".bin";
			jFusionFS.ffsWrite(thisPath, data);
		}
		long endTime = System.nanoTime();
		
		System.out.println("JFusionFS Created " + fileCount + " " + data.length + " byte files");
		System.out.println("Completion Time: " + elapsedTime(startTime, endTime) + " s");
	}
	
	private static void benchmarkVanillaWrite(int fileCount, byte[] data) {
		System.out.println("\n\nWriting Data With Java File IO");
		String filePath = config.getWorkspace() + "benchmark/vanilla_write_";
		String thisPath = "";
		
		long startTime = System.nanoTime();
		for(int i = 0; i < fileCount; i++) {
			thisPath = filePath + i + ".bin";
			vanillaWriteFile(thisPath, data);
		}
		long endTime = System.nanoTime();
		
		System.out.println("Java File IO Created " + fileCount + " " + data.length + " byte files");
		System.out.println("Completion Time: " + elapsedTime(startTime, endTime) + " s");
	}
	
	private static JFusionFSInstance instantiateJFusion() {
		System.out.println("Instantiating JFusionFS Instance");
		
		long startTime = System.nanoTime();
		JFusionFSInstance jFusionFS = new JFusionFSInstance();
		long endTime = System.nanoTime();
		
		System.out.println("Instantiation Time: " + elapsedTime(startTime, endTime) + " s");
		
		return jFusionFS;
	}
	
	private static void vanillaWriteFile(String path, byte[] data) {
		try {
			File file = new File(path);
			BufferedOutputStream output = new BufferedOutputStream(new FileOutputStream(file));
			output.write(data);
			output.close();
		} catch(IOException e) {
			e.printStackTrace();
		}
	}
	
	private static double elapsedTime(long startTime, long endTime) {
		long elapsedTime = endTime - startTime;
		double seconds = (double)elapsedTime / 1000000000.0;
		return seconds;
	}
	
	private static ArrayList<byte[]> getData(int size) {
		ArrayList<byte[]> data = new ArrayList<byte[]>();
		
		data.add(generator.getByteArrayOfSize(size));
		
		return data;
	}

}
