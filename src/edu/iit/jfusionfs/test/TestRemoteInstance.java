package edu.iit.jfusionfs.test;

import java.io.BufferedInputStream;

import edu.iit.jfusionfs.lib.JFusionFSInstance;

public class TestRemoteInstance {
	private static final String TEST_FILE_PATH = "testing/local_instance.bin";
	private static JFusionFSInstance jFusionFS;
	
	public static void main(String[] args) {
		printTestHeader();
		System.out.println("[+] Instantiating JFusionFS Instance");
		jFusionFS = new JFusionFSInstance();
		System.out.println("[+] JFusionFS Instance Initialized\n");
		
		System.out.println("[ffsRead] Reading File '" + TEST_FILE_PATH +"'");
		readData();
		System.out.println("[ffsRead] File Successfully Pulled\n");
	}
	
	private static BufferedInputStream readData() {
		return jFusionFS.ffsRead(TEST_FILE_PATH);
	}
	
	private static void printTestHeader() {
		String header = "";
		
		header += "Test JFusionFS Remote File Access";
		header += "\n\nConfig Requirements:";
		header += "\n   workspace=<directory on local machine>";
		header += "\n   maxlocalsize=1";
		header += "\n   distributedhashtable=edu.iit.jfusionfs.test.TestDHT";
		header += "\n\nSteps:";
		header += "\n   1. Configure As Above on Local Machine";
		header += "\n   2. Run TestLocalInstance on Remote Machine";
		header += "\n   3. Place Remote Machines IP in TestDHT file";
		header += "\n   4. Run JFusionFS on Remote Machine (The File Transfer Daemon)";
		header += "\n   5. Run TestRemoteInstance on Local Machine";
		header += "\n   6. Ensure file <workspace>\\testing\\local_instance.bin was created on Local Machine";
		header += "\n\n-----BEGIN TEST-----\n";
		
		System.out.println(header);
	}

}
