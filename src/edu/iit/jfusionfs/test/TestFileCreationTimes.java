package edu.iit.jfusionfs.test;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;

import edu.iit.jfusionfs.lib.JFusionFSInstance;

public class TestFileCreationTimes {
	private static final int[] SIZES = {10000, 100000, 1000000, 10000000, 100000000};
	private static final int MULTIPLIER = 4;
	private static final int ITERATIONS = 5;
	
	private static TestDataGenerator generator;
	
	private static ArrayList<Long> standardResults;
	private static ArrayList<Long> jFusionResults;
	
	public static void main(String[] args) {
		generator = new TestDataGenerator();
		
		standardResults = new ArrayList<Long>();
		jFusionResults = new ArrayList<Long>();
		
		ArrayList<byte[]> dataToWrite = getData();
		
		
		testFileCreation(dataToWrite);
		averageResults();
	}
	
	private static ArrayList<byte[]> getData() {
		ArrayList<byte[]> data = new ArrayList<byte[]>();
		
		for(int size : SIZES) {
			data.add(generator.getByteArrayOfSize(size));
		}
		
		return data;
	}
	
	private static void testFileCreation(ArrayList<byte[]> dataToWrite) {
		JFusionFSInstance jFusionFS = new JFusionFSInstance();
		int fileNumber = 1;
		
		System.out.println("\n------------JFusionFS------------");
		for(int i = 0; i < ITERATIONS; i++) {
			System.out.println("JFusionFS Library: Iteration " + Integer.toString(i));
			String filePath;
			Long startNano = System.nanoTime();
			for(int y = 0; y < MULTIPLIER; y++) {
				for(byte[] data : dataToWrite) {
					filePath = "testing\\jfusionLib_" + Integer.toString(fileNumber) + ".bin";
					jFusionFS.ffsWrite(filePath, data);
					fileNumber++;
				}
			}
			
			jFusionResults.add(System.nanoTime() - startNano);
		}
		
		System.out.println("\n------------Standard------------");
		for(int i = 0; i < ITERATIONS; i++) {
			System.out.println("Standard Library: Iteration " + Integer.toString(i+1));
			Long startNano = System.nanoTime();
			for(int y = 0; y < MULTIPLIER; y++) {
				for(byte[] data : dataToWrite) {
					File newFile = new File("Z:\\Data\\jfusion_workspace\\testing\\standardLib_" + Integer.toString(fileNumber) + ".bin");
					FileOutputStream output;
					try {
						output = new FileOutputStream(newFile);
						output.write(data);
					} catch (IOException e) {
						e.printStackTrace();
					}
					fileNumber++;
				}
			}
			
			standardResults.add(System.nanoTime() - startNano);
		}
		
	}
	
	private static void averageResults() {
		//Standard Library
		long standardTotal = 0; 
		for(long time : standardResults) {
			standardTotal += time;
		}
		long standardAverage = standardTotal / standardResults.size();
		
		long jFusionTotal = 0;
		for(long time : jFusionResults) {
			jFusionTotal += time;
		}
		long jFusionAverage = jFusionTotal / standardResults.size();
		
		System.out.println("\n\n------------RESULTS------------");
		System.out.println("Standard Library: " + Double.toString(standardAverage / 1000000000.0) + "s");
		System.out.println("JFusionFS Library: " + Double.toString(jFusionAverage / 1000000000.0) + "s");
	}

}
