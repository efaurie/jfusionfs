package edu.iit.jfusionfs.test;

import java.util.Random;

public class TestDataGenerator {
	
	Random generator;
	
	public TestDataGenerator() {
		generator = new Random();
	}
	
	public byte[] getByteArrayOfSize(int numberOfBytes) {
		byte[] randomData = new byte[numberOfBytes];
		generator.nextBytes(randomData);
		return randomData;
	}
}
