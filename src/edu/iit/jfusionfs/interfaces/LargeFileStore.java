package edu.iit.jfusionfs.interfaces;

import java.io.BufferedInputStream;
import java.io.File;

public interface LargeFileStore {

	void write(String filePath, byte[] data);
	
	BufferedInputStream read(String filePath);
	
	File open(String filePath);

}
