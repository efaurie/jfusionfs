package edu.iit.jfusionfs.interfaces;

public interface DistributedHashTable {
	
	void put(String filePath, String hostname, long size);
	
	String get(String filePath);

}
