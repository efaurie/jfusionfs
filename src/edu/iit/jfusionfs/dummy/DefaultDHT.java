package edu.iit.jfusionfs.dummy;

import java.util.HashMap;
import java.util.Map;

import edu.iit.jfusionfs.interfaces.DistributedHashTable;

public class DefaultDHT implements DistributedHashTable {
	
	Map<String, String> storedFiles;
	
	public DefaultDHT() {
		storedFiles = new HashMap<String, String>();
	}

	@Override
	public void put(String filePath, String hostname, long size) {
		String valueStore = hostname + ";" + Long.toString(size);
		storedFiles.put(filePath, valueStore);
	}

	@Override
	public String get(String filePath) {
		return storedFiles.get(filePath);
	}

}
