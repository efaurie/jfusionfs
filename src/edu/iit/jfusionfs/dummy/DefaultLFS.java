package edu.iit.jfusionfs.dummy;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

import edu.iit.jfusionfs.interfaces.LargeFileStore;

public class DefaultLFS implements LargeFileStore {
	
	private static final String TEST_DIRECTORY = "C:\\Users\\ericf_000\\Documents\\Data\\pfs_workspace\\";
	
	public DefaultLFS() {
		
	}
	
	private String getAbsolutePath(String filePath) {
		if(filePath.startsWith("\\"))
			filePath.replaceAll("^/+", "");
		
		return TEST_DIRECTORY + filePath;
	}
	
	private File getFileDescriptor(String filePath) {
		String completePath = getAbsolutePath(filePath);
		return new File(completePath);
	}

	@Override
	public void write(String filePath, byte[] data) {
		File file = getFileDescriptor(filePath);
		file.getParentFile().mkdirs();
		
		FileOutputStream output;
		try {
			output = new FileOutputStream(file);
			output.write(data);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	@Override
	public BufferedInputStream read(String filePath) {
		BufferedInputStream input = null;
		
		String absolutePath = getAbsolutePath(filePath);
		try {
			input = new BufferedInputStream(new FileInputStream(absolutePath));
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		
		return input;
	}

	@Override
	public File open(String filePath) {
		String absolutePath = getAbsolutePath(filePath);
		return new File(absolutePath);
	}

}
