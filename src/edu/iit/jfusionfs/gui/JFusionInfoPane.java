package edu.iit.jfusionfs.gui;

import java.awt.Color;
import java.awt.GridLayout;
import java.io.File;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.text.DecimalFormat;

import javax.swing.BorderFactory;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.border.Border;

public class JFusionInfoPane extends JPanel {
	private static final long serialVersionUID = 1L;
	
	private JTextArea networkInfoPane;
	private JTextArea diskInfoPane;
	private JTextArea jFusionInfoPane;
	
	public JFusionInfoPane() {
		init();
	}
	
	private void init() {
		networkInfoPane = new JTextArea(5, 5);
		diskInfoPane = new JTextArea(5, 5);
		jFusionInfoPane = new JTextArea(5, 5);
		
		Border border = BorderFactory.createLineBorder(Color.BLACK);
		networkInfoPane.setBorder(BorderFactory.createCompoundBorder(border, BorderFactory.createEmptyBorder(10, 10, 10, 10)));
		diskInfoPane.setBorder(BorderFactory.createCompoundBorder(border, BorderFactory.createEmptyBorder(10, 10, 10, 10)));
		jFusionInfoPane.setBorder(BorderFactory.createCompoundBorder(border, BorderFactory.createEmptyBorder(10, 10, 10, 10)));
		
		this.setLayout(new GridLayout(3, 1));
		this.add(jFusionInfoPane);
		this.add(networkInfoPane);
		this.add(diskInfoPane);
		
		updateInfo();
	}
	
	public void updateInfo() {
		String networkInfo = getNetworkInfo();
		String diskInfo = getDiskInfo();
		String jFusionInfo = getJFusionInfo();
		
		networkInfoPane.setText(networkInfo);
		diskInfoPane.setText(diskInfo);
		jFusionInfoPane.setText(jFusionInfo);
	}
	
	private String getNetworkInfo() {
		String networkInfo = "";
		
		String hostname;
		try {
			hostname = InetAddress.getLocalHost().toString().split("/")[1];
		} catch (UnknownHostException e) {
			hostname = "Unknown";
		}
		
		networkInfo += "\nHostname: " + hostname;
		
		return networkInfo;
	}
	
	private String getDiskInfo() {
		String diskInfo = "";
		
		File[] systemRoots = File.listRoots();
		DecimalFormat percentage = new DecimalFormat("###.##");
		
		for(File systemRoot : systemRoots) {
			if(systemRoot.getTotalSpace() == 0)
				continue;
			
			diskInfo += "\nDisk: " + systemRoot.getAbsolutePath();
			diskInfo += "\nFree Space: " + percentage.format((((float)systemRoot.getFreeSpace() / systemRoot.getTotalSpace())*100)) + "%";
			diskInfo += "\nCapacity (free/total): " + getReadableFileSize(systemRoot.getFreeSpace()) + " / " + getReadableFileSize(systemRoot.getTotalSpace());
			diskInfo += "\n";
		}
		
		return diskInfo;
	}
	
	public String getReadableFileSize(long size) {
	    if(size <= 0)
	    	return "0";
	    
	    final String[] units = new String[] { "B", "kB", "MB", "GB", "TB" };
	    int digitGroups = (int) (Math.log10(size)/Math.log10(1024));
	    
	    return new DecimalFormat("#,##0.#").format(size/Math.pow(1024, digitGroups)) + " " + units[digitGroups];
	}
	
	private String getJFusionInfo() {
		return "JFusion Version 1.0";
	}
	
}
