package edu.iit.jfusionfs.gui;

import java.awt.BorderLayout;
import java.io.PrintStream;

import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;

public class JFusionOutputPane extends JPanel {
	private static final long serialVersionUID = 1L;
	
	private JTextArea terminal;
	private JFusionOutputStream outputStream;
	
	public JFusionOutputPane() {
		init();
		registerOutputStream();
	}
	
	private void init() {
		this.setLayout(new BorderLayout());
		
		terminal = new JTextArea(30, 100);
		terminal.setEditable(false);
		
		outputStream = new JFusionOutputStream(terminal);
		
		JScrollPane scrollPane = new JScrollPane(terminal, JScrollPane.VERTICAL_SCROLLBAR_ALWAYS, JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
		add(scrollPane);
	}
	
	private void registerOutputStream() {
		System.setOut(new PrintStream(outputStream));
	}
}
