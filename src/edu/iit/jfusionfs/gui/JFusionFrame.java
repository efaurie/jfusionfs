package edu.iit.jfusionfs.gui;

import java.awt.BorderLayout;

import javax.swing.JFrame;

public class JFusionFrame extends JFrame {
	private static final long serialVersionUID = 1L;
	
	private JFusionInfoPane info;
	private JFusionOutputPane terminal;
	
	public JFusionFrame() {
		super("JFusionFS Terminal");
		init();
	}
	
	private void init() {
		this.setLocationRelativeTo(null);
		this.setLayout(new BorderLayout());
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		info = new JFusionInfoPane();
		terminal = new JFusionOutputPane();
		
		this.add(info, BorderLayout.EAST);
		this.add(terminal, BorderLayout.CENTER);
		this.pack();
	}
	
	public void display() {
		this.setVisible(true);
	}
}
