package edu.iit.jfusionfs.amazon;

import com.amazonaws.auth.profile.ProfileCredentialsProvider;
import com.amazonaws.regions.Region;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDBClient;
import com.amazonaws.services.dynamodbv2.document.DynamoDB;
import com.amazonaws.services.dynamodbv2.document.Item;
import com.amazonaws.services.dynamodbv2.document.Table;

import edu.iit.jfusionfs.interfaces.DistributedHashTable;

public class DynamoDHT implements DistributedHashTable {
	private static final String TABLE_NAME = "jfusionfs";
	private static final Region REGION = Region.getRegion(Regions.US_WEST_2); 
	
	private DynamoDB dynamoDB;
	private Table table;
	
	public DynamoDHT() {
		initConnection();
	}
	
	private void initConnection() {
		AmazonDynamoDBClient client = new AmazonDynamoDBClient(new ProfileCredentialsProvider());
		client.setRegion(REGION);
		
		dynamoDB = new DynamoDB(client);
		table = dynamoDB.getTable(TABLE_NAME);
	}

	@Override
	public void put(String filePath, String hostname, long size) {
		String value = hostname + ";" + Long.toString(size);
		
		Item newEntry = new Item()
			.withPrimaryKey("filepath", filePath)
			.withString("value", value);
		
		table.putItem(newEntry);
	}

	@Override
	public String get(String filePath) {
		Item item = table.getItem("filepath", filePath);
		return (String) item.get("value");
	}

}
