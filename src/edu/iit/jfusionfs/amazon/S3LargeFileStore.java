package edu.iit.jfusionfs.amazon;

import java.io.BufferedInputStream;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;

import com.amazonaws.auth.profile.ProfileCredentialsProvider;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3Client;
import com.amazonaws.services.s3.model.GetObjectRequest;
import com.amazonaws.services.s3.model.ObjectMetadata;
import com.amazonaws.services.s3.model.S3Object;

import edu.iit.jfusionfs.interfaces.LargeFileStore;

public class S3LargeFileStore implements LargeFileStore {
	private static final String BUCKET_NAME = "jfusionfsworkspace";
	
	private String workspace;
	private AmazonS3 s3Client;
	
	public S3LargeFileStore(String workspace) {
		this.workspace = workspace;
		initConnection();
	}
	
	private void initConnection() {
		s3Client = new AmazonS3Client(new ProfileCredentialsProvider());
	}
	
	private String getAbsolutePath(String filePath) {
		if(filePath.startsWith("/"))
			filePath.replaceAll("^/+", "");
		
		return workspace + filePath;
	}

	@Override
	public void write(String filePath, byte[] data) {
		InputStream stream = new ByteArrayInputStream(data);
		
		ObjectMetadata metadata = new ObjectMetadata();
		metadata.setContentLength(data.length);
		
		s3Client.putObject(BUCKET_NAME, filePath, stream, metadata);
	}

	@Override
	public BufferedInputStream read(String filePath) {
		S3Object s3FileObject = s3Client.getObject(new GetObjectRequest(BUCKET_NAME, filePath));
		BufferedInputStream fileStream = new BufferedInputStream(s3FileObject.getObjectContent());
		
		return fileStream;
	}

	@Override
	public File open(String filePath) {
		int bytesRead;
		byte[] buffer = new byte[5 * 1024];
				
		BufferedInputStream input = read(filePath);
		File file = new File(getAbsolutePath(filePath));
		file.getParentFile().mkdirs();
		
		try {
			FileOutputStream output = new FileOutputStream(file);
			
			while ((bytesRead = input.read(buffer)) != -1) {
				output.write(buffer, 0, bytesRead);
			}
		
			output.close();
		} catch(IOException e) {
			e.printStackTrace();
		}
		
		return file;
	}
}
