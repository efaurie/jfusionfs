package edu.iit.jfusionfs.lib;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;

public class DynamicClassFactory<E> {
	private String workspace;
	private Class loadedClass;
	private Constructor constructor;
	
	public DynamicClassFactory(String classBinName, String workspace) {
		this.workspace = workspace;
		loadClass(classBinName);
	}
	
	private void loadClass(String classBinName) {
		ClassLoader classLoader = this.getClass().getClassLoader();
		try {
			loadedClass = classLoader.loadClass(classBinName);
			constructor = loadedClass.getConstructor();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} catch(NoSuchMethodException e) {
			try {
				constructor = loadedClass.getDeclaredConstructor(String.class);
			} catch (NoSuchMethodException e1) {
				e1.printStackTrace();
			} catch (SecurityException e1) {
				e1.printStackTrace();
			}
		}
	}
	
	public E getInstance() {
		Object classInstance = null;
		
		try {
			Class[] args = constructor.getParameterTypes();
			if(args.length > 0 && args[0].getName() == "java.lang.String")
				classInstance = constructor.newInstance(workspace);
			else
				classInstance = constructor.newInstance();
		} catch(InvocationTargetException e) {
			e.printStackTrace();
		} catch(IllegalAccessException e) {
			e.printStackTrace();
		} catch(InstantiationException e) {
			e.printStackTrace();
		}
		
		return (E)classInstance;
	}
}
