package edu.iit.jfusionfs.lib;

import java.io.BufferedInputStream;
import java.io.File;

import edu.iit.jfusionfs.interfaces.DistributedHashTable;
import edu.iit.jfusionfs.interfaces.LargeFileStore;

public class JFusionFSInstance {
	
	private JFusionConfigReader config; 
	
	private JFusionFileWriter writer;
	private JFusionFileReader reader;
	
	
	public JFusionFSInstance() {
		config = new JFusionConfigReader();
		initComponents();
		
	}
	
	public JFusionFSInstance(String configPath) {
		config = new JFusionConfigReader(configPath);
		initComponents();
	}
	
	private void initComponents() {
		String largeFileStoreClass = config.getLargeFileStore();
		String distributedHashTableClass = config.getDistributedHashTable();
		
		LargeFileStore largeFileStore = new DynamicClassFactory<LargeFileStore>(largeFileStoreClass, config.getWorkspace()).getInstance();
		DistributedHashTable distributedHashTable = new DynamicClassFactory<DistributedHashTable>(distributedHashTableClass, config.getWorkspace()).getInstance();
		
		writer = new JFusionFileWriter(config, largeFileStore, distributedHashTable);
		reader = new JFusionFileReader(config, largeFileStore, distributedHashTable);
	}
	
	public void ffsWrite(String filePath, byte[] data) {
		writer.write(filePath, data);
	}
	
	public void ffsAppend(String filePath, byte[] data) {
		// Reading will ensure the file is on the local host
		reader.read(filePath);
		writer.append(filePath, data);
	}
	
	public BufferedInputStream ffsRead(String filePath) {
		return reader.read(filePath);
	}
	
	public File ffsOpen(String filePath) {
		return reader.open(filePath);
	}
}
