package edu.iit.jfusionfs.lib;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

public class FileTransferDaemon implements Runnable {
	
	private int port;
	private String workspace;
	private ServerSocket socket;

	public FileTransferDaemon(int port, String workspace) {
		this.port = port;
		this.workspace = workspace;
	}
	
	private Socket listenForConnection() {
		Socket clientSocket = null;
		
		try {
			clientSocket = socket.accept();
		} catch (IOException e) {
			System.out.println("[-] Failed to accept new client connection!");
		}
		
		System.out.println("[+] Connection request recieved from Client: " + clientSocket.getRemoteSocketAddress().toString().replaceAll("^/+", ""));
		return clientSocket;
	}
	
	private void initServer() {
		System.out.println("[+] Initializing File Transfer Daemon on port " + port);
		try {
			socket = new ServerSocket(port);
			
		} catch(IOException e) {
			System.out.println("[-] Failed to start File Transfer Daemon!");
		}
	}
	
	@Override
	public void run() {
		initServer();
		System.out.println("[+] Listening for connection request on Port: " + port);
		
		while(true) {
			Socket clientSocket = listenForConnection();
			if(clientSocket != null)
				new Thread(new FileTransferThread(clientSocket, workspace)).start();
		}
	}

}
