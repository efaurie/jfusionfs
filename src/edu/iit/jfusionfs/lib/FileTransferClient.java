package edu.iit.jfusionfs.lib;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.net.Socket;

public class FileTransferClient {
	private static final int BUFFER_SIZE = 1024;
	
	private Socket socket;
	private PrintWriter output;
	private InputStream input;
	
	public FileTransferClient(String fileHost, int port) {
		try {
			socket = new Socket(fileHost, port);
			output = new PrintWriter(socket.getOutputStream(), true);
			input = socket.getInputStream();
		} catch(IOException e) {
			e.printStackTrace();
		}
	}
	
	public File getFile(String filePath, String localPath) {
		//Initialize Directory Structure
		File file = new File(localPath);
		file.getParentFile().mkdirs();
		
		try {
			//Send the request to the File's Host
			String request = "GET;" + filePath;
			output.println(request);
			
			FileOutputStream fileOut = new FileOutputStream(localPath);
			
			int bytesRead; 
			byte[] buffer = new byte[BUFFER_SIZE];
			while((bytesRead = input.read(buffer)) != -1) {
				fileOut.write(buffer, 0, bytesRead);
			}
			
			fileOut.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch(IOException e) {
			e.printStackTrace();
		}
		
		return file;
	}

}
