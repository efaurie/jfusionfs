package edu.iit.jfusionfs.lib;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class JFusionConfigReader {
	private static final String DEFAULT_CONFIG = JFusionConfigReader.class.getClassLoader().getResource("edu/iit/jfusionfs/resources/jfusionfs.conf").getPath();
	
	private String configPath;
	private Map<String, String> parameters;
	private ArrayList<String> hosts;
	
	public JFusionConfigReader() {
		this.configPath = DEFAULT_CONFIG;
		init();
	}

	public JFusionConfigReader(String configPath) {
		this.configPath = configPath;
		init();
	}
	
	private void init() {
		parameters = new HashMap<String, String>();
		hosts = new ArrayList<String>();
		
		parseConfig();
	}
	
	private void parseConfig() {
		System.out.println("[+] Parsing Config: " + configPath.replaceAll("^/+", ""));
		
		try {
			BufferedReader reader = new BufferedReader(new FileReader(configPath));
			String currentLine;
			
			while((currentLine = reader.readLine()) != null) {
				if(currentLine.startsWith("#") || currentLine.equals(""))
					continue;
				
				String[] parameter = currentLine.split("=");
				switch(parameter[0]) {
					case "hosts":
						parseHosts(parameter[1]);
					default:
						parameters.put(parameter[0], parameter[1]);
				}
			}
			
			reader.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
			System.out.println("[-] Cannot continue, config not found!");
		} catch (IOException e) {
			e.printStackTrace();
			System.out.println("[-] Cannot continue, config malformed!");
		}
		
	}
	
	private void parseHosts(String parameter) {
		String[] hostArray = parameter.split(",");
		
		for(String host : hostArray) {
			hosts.add(host.replaceAll("\\s", ""));
		}
	}
	
	public ArrayList<String> getHosts() {
		return hosts;
	}
	
	public int getFileTransferPort() {
		return Integer.parseInt(parameters.get("port"));
	}
	
	public String getWorkspace() {
		String workspace = parameters.get("workspace");
		
		if(!workspace.endsWith("/"))
			workspace = workspace + "/";
		
		return workspace;
	}
	
	public int getMaxLocalFileSize() {
		return Integer.parseInt(parameters.get("maxlocalsize"));
	}
	
	public double getDiskUseThreshold() {
		return Double.parseDouble(parameters.get("diskusethreshold"));
	}
	
	public String getLargeFileStore() {
		return parameters.get("largefilestore");
	}
	
	public String getDistributedHashTable() {
		return parameters.get("distributedhashtable");
	}
}
