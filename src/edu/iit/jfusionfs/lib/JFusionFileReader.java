package edu.iit.jfusionfs.lib;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.net.InetAddress;
import java.net.UnknownHostException;

import edu.iit.jfusionfs.interfaces.DistributedHashTable;
import edu.iit.jfusionfs.interfaces.LargeFileStore;

public class JFusionFileReader {
	
	private String hostname;
	private String workspace;
	JFusionConfigReader config;
	private LargeFileStore parallelFileSystem;
	private DistributedHashTable distributedHashTable;
	
	public JFusionFileReader(JFusionConfigReader config, LargeFileStore parallelFileSystem, DistributedHashTable distributedHashTable) {
		hostname = getHostname();
		workspace = config.getWorkspace();
		this.config = config;
		this.parallelFileSystem = parallelFileSystem;
		this.distributedHashTable = distributedHashTable;
	}
	
	public BufferedInputStream read(String filePath) {
		String fileHost = getFileHost(filePath);
		
		if(fileHost == null)
			return null;
		if(hostname.equals(fileHost))
			return readLocally(filePath);
		else if(fileHost.equals("LFS"))
			return readFromPFS(filePath);
		else
			return readRemotely(filePath, fileHost);
	}
	
	public File open(String filePath) {
		String fileHost = getFileHost(filePath);
		
		if(fileHost == null)
			return null;
		if(hostname.equals(fileHost))
			return openLocally(filePath);
		else if(fileHost.equals("LFS"))
			return openFromPFS(filePath);
		else
			return openRemotely(filePath, fileHost);
	}
	
	private String getAbsolutePath(String filePath) {
		if(filePath.startsWith("/"))
			filePath.replaceAll("^/+", "");
		
		return workspace + filePath;
	}
	
	public BufferedInputStream readLocally(String filePath) {
		BufferedInputStream input = null;
		String absolutePath = getAbsolutePath(filePath);
		
		try {
			input = new BufferedInputStream(new FileInputStream(absolutePath));
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		
		return input;
	}
	
	public BufferedInputStream readRemotely(String filePath, String fileHost) {
		BufferedInputStream input = null;
		File remoteFile = retrieveFile(filePath, fileHost);
		
		try {
			input = new BufferedInputStream(new FileInputStream(remoteFile.getAbsolutePath()));
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		
		return input;
	}
	
	public BufferedInputStream readFromPFS(String filePath) {
		return parallelFileSystem.read(filePath);
	}
	
	public File openLocally(String filePath) {
		return new File(getAbsolutePath(filePath));
	}
	
	public File openRemotely(String filePath, String fileHost) {
		return retrieveFile(filePath, fileHost);
	}
	
	public File openFromPFS(String filePath) {
		return parallelFileSystem.open(filePath);
	}
	
	private String getFileHost(String filePath) {
		String valueStored = distributedHashTable.get(filePath);
		return valueStored.split(";")[0];
	}
	
	private String getHostname() {
		String hostname = "";
		
		try {
			hostname = InetAddress.getLocalHost().toString().split("/")[1];
		} catch (UnknownHostException e) {
			e.printStackTrace();
		}
		
		return hostname;
	}
	
	private File retrieveFile(String filePath, String fileHost) {
		FileTransferClient ftpClient = new FileTransferClient(fileHost, config.getFileTransferPort());
		return ftpClient.getFile(filePath, getAbsolutePath(filePath));
	}
}
