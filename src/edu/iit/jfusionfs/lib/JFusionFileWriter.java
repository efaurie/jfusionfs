package edu.iit.jfusionfs.lib;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.nio.file.Path;
import java.nio.file.Paths;

import edu.iit.jfusionfs.interfaces.DistributedHashTable;
import edu.iit.jfusionfs.interfaces.LargeFileStore;

public class JFusionFileWriter {
	
	private String hostname;
	private String workspace;
	private JFusionConfigReader config;
	private LargeFileStore largeFileStore;
	private DistributedHashTable distributedHashTable;
	
	public JFusionFileWriter(JFusionConfigReader config, LargeFileStore largeFileStore, DistributedHashTable distributedHashTable) {
		hostname = getHostname();
		workspace = config.getWorkspace();
		this.config = config;
		this.largeFileStore = largeFileStore;
		this.distributedHashTable = distributedHashTable;
	}
	
	public void write(String filePath, byte[] data) {
		if(shouldStoreLocally(data.length)) {
			writeLocally(filePath, data);
			distributedHashTable.put(filePath, hostname, (long)data.length);
		} else {
			writeToLargeFileStore(filePath, data);
			distributedHashTable.put(filePath, "LFS", (long)data.length);
		}	
	}
	
	public void append(String filePath, byte[] data) {
		File file = getFileDescriptor(filePath);
		appendDataToLocalFile(file, data);
		distributedHashTable.put(filePath, hostname, file.length());
	}
	
	private File writeLocally(String filePath, byte[] data) {
		File file = getFileDescriptor(filePath);
		writeDataToLocalDisk(file, data);
		
		return file;
	}
	
	private void writeToLargeFileStore(String filePath, byte[] data) {
		largeFileStore.write(filePath, data);
	}
	
	private File getFileDescriptor(String filePath) {
		if(filePath.startsWith("/"))
			filePath.replaceAll("^/+", "");
		
		String completePath = workspace + filePath;
		
		return new File(completePath);
	}
	
	private void writeDataToLocalDisk(File file, byte[] data) {
		file.getParentFile().mkdirs();
		
		FileOutputStream output;
		try {
			output = new FileOutputStream(file);
			output.write(data);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	private void appendDataToLocalFile(File file, byte[] data) {
		FileOutputStream output;
		
		try{
			output = new FileOutputStream(file, true);
			output.write(data);
			output.flush();
			output.close();
		} catch(FileNotFoundException e) {
			e.printStackTrace();
		} catch(IOException e) {
			e.printStackTrace();
		}
	}
	
	private boolean spaceAvailable(int numberOfBytes) {
		Path workspacePath = Paths.get(workspace);
		File root = workspacePath.getRoot().toFile();
		
		double diskUseThreshold = (double)config.getDiskUseThreshold() / 100;
		double diskUseLimit = root.getTotalSpace() * diskUseThreshold;
		double currentDiskUse = root.getTotalSpace() - root.getFreeSpace();
		
		if(currentDiskUse + numberOfBytes > diskUseLimit)
			return false;
		
		return true;
	}
	
	private boolean shouldStoreLocally(int numberOfBytes) {
		int maxLocalFileSize = config.getMaxLocalFileSize();
		if(maxLocalFileSize > 0 && numberOfBytes > (maxLocalFileSize*1000000))
			return false;
		else if(!spaceAvailable(numberOfBytes))
			return false;
		
		return true;
	}
	
	private String getHostname() {
		String hostname = "";
		
		try {
			hostname = InetAddress.getLocalHost().toString().split("/")[1];
		} catch (UnknownHostException e) {
			e.printStackTrace();
		}
		
		return hostname;
	}

}
