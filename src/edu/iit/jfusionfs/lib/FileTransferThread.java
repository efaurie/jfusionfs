package edu.iit.jfusionfs.lib;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.Socket;

public class FileTransferThread implements Runnable {
	
	private Socket socket;
	private OutputStream output;
	private BufferedReader input;
	private String workspace;
	private String clientID;

	public FileTransferThread(Socket socket, String workspace) {
		try {
			this.socket = socket;
			this.workspace = workspace;
			clientID = socket.getRemoteSocketAddress().toString().replaceAll("^/+", "");
			
			output = socket.getOutputStream();
			input = new BufferedReader(new InputStreamReader(socket.getInputStream()));
		} catch(IOException e) {
			System.out.println("[-] (Client: " + clientID + ") File Transfer Thread failed to initialize!");
		}
	}
	
	private String listenForRequest() {
		try {
			return input.readLine();
		} catch(IOException e) {
			System.out.println("[-] (Client: " + clientID + ") Failed to receive request. Disconnecting...");
			return "";
		}
	}
	
	private void sendFile(String filename) {
		System.out.println("[+] (Client: " + clientID + ") Attempting to send file: '" + workspace + filename + "'");
		try {
			File sendFile = new File(workspace + filename);
			FileInputStream fileStream = new FileInputStream(sendFile);
			BufferedInputStream bufferedFileStream = new BufferedInputStream(fileStream);
		
			byte[] byteArray = new byte[(int) sendFile.length()];
			
			bufferedFileStream.read(byteArray, 0, byteArray.length);
			
			output.write(byteArray, 0, byteArray.length);
			output.flush();
			
			bufferedFileStream.close();
			System.out.println("[+] (Client: " + clientID + ") File sent successfully.");
		} catch(FileNotFoundException e) {
			System.out.println("[-] (Client: " + clientID + ") Process Request: File '" + filename + "' not found!");
		} catch(IOException e) {
			System.out.println("[-] (Client: " + clientID + ") Failed to stream file '" + filename + "'!");
		}
	}
	
	private void processRequest(String request) {
		String[] parameters = request.split(";");
		switch(parameters[0]) {
			case "DISCONNECT":
				break;
			case "GET":
				sendFile(parameters[1]);
				break;
			default:
				break;
		}
	}
	
	
	@Override
	public void run() {		
		String request = listenForRequest();
		
		if(!request.equals(""))
			processRequest(request);
		
		System.out.println("[+] (Client: " + clientID + ") Terminating Transfer Thread.");
		try {
			socket.close();
		} catch (IOException e) {
			System.out.println("[-] (Client: " + clientID + ") Failed to close socket!");
		}
	}

}
